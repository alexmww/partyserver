package ru.nsu.shift.server;

import ru.nsu.shift.server.model.Profile;

public class TestHelper {

    public static Profile getFilledProfile() {
        Profile profile = new Profile();
        profile.setAge(1);
        profile.setFirstName("Test");
        profile.setLastName("Test");
        profile.setImage("c===3");
        return profile;
    }
}
