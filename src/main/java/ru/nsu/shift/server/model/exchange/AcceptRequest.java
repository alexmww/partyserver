package ru.nsu.shift.server.model.exchange;

import lombok.Data;

import java.io.Serializable;

@Data
public class AcceptRequest implements Serializable {
    /**
     * id куда принимаем заявку
     */
    private Long eventId;
    /**
     * от кого принимаем заявку
     */
    private Long userId;
}
